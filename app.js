/* ------------------------------------------
 * Catmull Subdivision (not working)
 -------------------------------------------*/
function HalfEdge(opp, next, end, left) {
	this.opp  = null || opp;
	this.next = null || next;
	this.end  = null || end;
	this.left = null || left;

	/*opp: null,  // HE
	next: null, // HE
	end: null,  // V
	left: null  // F*/
}

function Vertex(x, y, z, edge) {
	this.x = null || x;
	this.y = null || y;
	this.z = null || z;

	this.edge = null || edge;

	/*x: undefined,
	y: undefineda);
	z: undefined,
	edge: null*/
}

Vertex.sum = function(o, a, b) {
	o.x = a.x + b.x;
	o.y = a.y + b.y;
	o.z = a.z + b.z;

	return o;
};

Vertex.mulByScalar = function(o, a, s) {
	o.x = a.x * s;
	o.y = a.y * s;
	o.z = a.z * s;

	return o;
};

Vertex.divByScalar = function(o, a, s) {
	o.x = a.x / s;
	o.y = a.y / s;
	o.z = a.z / s;

	return o;
};

function Face(edge) {
	this.edge = edge;

	/*edge: null*/
}

function subdivide(vertices, indices) {
	// #### Main

	// #Globals
	var vos = {}; // vertex objects
	var fos = {}; // face objects
	var eos = {}; // edge objects

	var svs =[];   // subdivided vertices
	var sqis = []; // subdivided quad indices
	var stis = []; // subdivided triangle indices

	constructHalfEdgeStruct();
	genSubdivisionPoints();
	genQuadIndices();
	genTriangleIndices();

	return {'vertices': svs, 'quadIndices': sqis, 'triangleIndices': stis};

	// #### Helper Function definitions
	function constructHalfEdgeStruct() {
		processVertices();
		processIndices();

		function processVertices() {
			// #Create vertices
			// number of vertices
			var svs = vertices.length / 3;

			for (var vi = 0; vi < svs; vi++) {
				// start of i'th vertex's components (in this case i = vi for 'vertex index')
				var sv = 3*vi;
				vos[vi] = new Vertex(vertices[sv], vertices[sv+1], vertices[sv+2]);
			}
		}

		function processIndices() {
			// #Create half-edges, and in turn faces
			// number of faces
			var nfs = indices.length / 4;

			for (var fi = 0; fi < nfs; fi++) {
				// start of i'th face's vertices (in this case i = fi for 'face index')
				var sfv = 4*fi;
				// array of i'th face's edges, four total each an array of vertex pairs
				var fes = [];

				for (var fvi = 0; fvi < 4; fvi++) {
					// first and second vertex pair
					var fvp = fvi;
					var svp = (fvi+1) % 4;

					fes.push([indices[sfv+fvp], indices[sfv+svp]]);
				}

				// create face
				var f = new Face();
				fos[fi] = f;
				// number of edges for current face (should always be 4)
				var nfes = fes.length;

				for (var ei = 0; ei < nfes; ei++) {
					var eki = fes[ei].toString();

					eos[eki] = new HalfEdge();
					eos[eki].left = f;
					eos[eki].end = vos[fes[ei][1]];
				}

				// assign .edge of face to arbitrary bordering edge (here, first created)
				f.edge = eos[fes[0].toString()];

				for (var ej = 0; ej < nfes; ej++) {
					var ekj = fes[ej].toString();

					// ##assign .edge to bordering face vertices if not done so yet

					// select vertex used as edge's starting point
					var vo = vos[fes[ej][0]];
					if (!vo.edge) {
						vo.edge = eos[ekj];
					}

					// ##assign .next and .opp to edge

					// next edge index
					var nei = (ej+1) % 4;
					// key for next edge in eos
					var nek = fes[nei].toString();

					eos[ekj].next = eos[nek];

					// opposite edge key
					var oek = [fes[ej][1], fes[ej][0]].toString();

					// if opposite edge exists, assign .opp of current edge to it and vice versa
					if (eos[oek]) {
						eos[ekj].opp = eos[oek];
						eos[oek].opp = eos[ekj];
					}
				}
			}
		}
	}

	function genSubdivisionPoints() {
		// global new vertex index
		var gnvi = 0;

		genFacePoints();
		genEdgePoints();
		genNewPoints();

		function addPoint(point) {
			svs.push(point.x, point.y, point.z);
			return {'v': point, 'i': gnvi++};
		}

		function genFacePoints() {
			// #for each face, add a face-point

			for (var fk in fos) {
				if (!fos.hasOwnProperty(fk)) { continue; }

				var vs = [];

				var f = fos[fk];
				var e = f.edge;
				vs.push(e.end);

				var it = e.next;
				while (it !== e) {
					vs.push(it.end);
					it = it.next;
				}

				var nfvs = vs.length;
				var fp = new Vertex(0, 0, 0);

				for (var i = 0; i < nfvs; i++) {
					Vertex.sum(fp, fp, vs[i]);
				}

				Vertex.divByScalar(fp, fp, nfvs);

				f.facePoint = addPoint(fp);
			}
		}

		function genEdgePoints() {
			// #for each edge, add an edge-point

			for (var hek in eos) {
				if (!eos.hasOwnProperty(hek)) { continue; }
				var he = eos[hek];

				if (he.opp.edgePoint) {
					he.edgePoint = he.opp.edgePoint;
					continue;
				}

				// two incident vertices
				var tp = he.end;
				var ip = he.opp.end;

				// two face points from incident faces
				var fp1 = he.left.facePoint;
				var fp2 = he.opp.left.facePoint;

				// calculate edge point
				var ep = new Vertex(0, 0, 0);
				Vertex.sum(ep, ep, tp);
				Vertex.sum(ep, ep, ip);
				Vertex.sum(ep, ep, fp1);
				Vertex.sum(ep, ep, fp2);
				Vertex.divByScalar(ep, ep, 4);

				he.edgePoint = addPoint(ep);
			}
		}

		function genNewPoints() {
			// #for each original point:

			for (var vk in vos) {
				if (!vos.hasOwnProperty(vk)) { continue; }
				//console.log("for vertex: " + vk);
				var v = vos[vk];

				// collect current point's adjacent faces (adjfs) and edges (adjes)
				var adjfs = [];
				var adjes = [];

				var he = v.edge;
				adjfs.push(he.left);
				adjes.push(he);

				var it = he.opp.next;
				while (it !== he) {
					adjfs.push(it.left);
					adjes.push(it);
					it = it.opp.next;
				}

				// #average F of all n face points for faces touching v
				var nadjfs = adjfs.length;
				//console.log('    num adj fs:' + nadjfs);
				var F = new Vertex(0, 0, 0);

				for (var i = 0; i < nadjfs; i++) {
					Vertex.sum(F, F, adjfs[i].facePoint);
				}

				Vertex.divByScalar(F, F, nadjfs);

				// average R of all n edge midpoints for edges touching v
				var nadjes = adjes.length; // valence
				//console.log('    num adj es:' + nadjes);
				var R = new Vertex(0, 0, 0);

				for (var j = 0; j < nadjes; j++) {
					var tp = adjes[j].end;
					var ip = adjes[j].opp.end;

					var mp = new Vertex(0, 0, 0);
					Vertex.sum(mp, tp, ip);
					Vertex.divByScalar(mp, mp, 2);

					Vertex.sum(R, R, mp);
				}

				Vertex.divByScalar(R, R, nadjes);

				// generate new point, via (F + 2*R + (n-3)*P) / n
				var np = new Vertex(0, 0, 0);

				Vertex.sum(np, np, F);

				Vertex.mulByScalar(R, R, 2);
				Vertex.sum(np, np, R);

				// TODO: Might have to change, n=valence
				// (n-3)*P, since n=4 skip this step
				Vertex.sum(np, np, v);

				Vertex.divByScalar(np, np, 4);

				v.newPos = addPoint(np);
			}
		}
	}

	function genQuadIndices() {
		for (var fk in fos) {
			if (!fos.hasOwnProperty(fk)) { continue; }
			// for every face
			var f = fos[fk];

			// iterate over its half-edges
			var he = f.edge;
			var it = he;

			// and connect appropriate points
			do {
				sqis.push(
						it.edgePoint.i,
						f.facePoint.i,
						it.opp.next.edgePoint.i,
						it.opp.end.newPos.i
				);

				it = it.next;
			} while (it !== he);
		}
	}

	function genTriangleIndices() {
		for (var i = 0; i < sqis.length/4; i++) {
			var start = 4*i;

			stis.push(sqis[start], sqis[start+1], sqis[start+3]);
			stis.push(sqis[start+1], sqis[start+2], sqis[start+3]);
		}
	}
}

/* ------------------------------------------
 * Global Vars
 -------------------------------------------*/
var mvMatrix = mat4.create();
var pMatrix = mat4.create();
var nMatrix = mat4.create();

var ANGLE_STEP = 45.0;
var then = Date.now();

function main() {
	// Retrieve <canvas> element
	var canvas = document.getElementById("webgl");

	// Get the rendering context for WebGL
	var gl = getWebGLContext(canvas);

	// Initialize shaders
	var vShaderSource = getShaderSource("shader-vs");
	var fShaderSource = getShaderSource("shader-fs");
	if (!initShaders(gl, vShaderSource, fShaderSource)) {
		console.log("Failed to initialize shaders");
		return;
	}

	// Set the clear color and enable the depth test
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	gl.clearColor(0.15, 0.2, 0.25, 1.0);
	gl.enable(gl.DEPTH_TEST);

	// Get the storage locations of attribute and uniform variables
	var program = gl.program;

	// attribute variables
	program.a_Position = gl.getAttribLocation(program, 'a_position');
	program.a_textureCoord = gl.getAttribLocation(program, 'a_textureCoord');
	program.a_vertexNormal = gl.getAttribLocation(program, 'a_vertexNormal');

	// uniform variables
	program.u_mvMatrix = gl.getUniformLocation(program, 'u_mvMatrix');
	program.u_pMatrix = gl.getUniformLocation(program, 'u_pMatrix');
	program.u_nMatrix = gl.getUniformLocation(program, 'u_nMatrix');

	program.u_sampler = gl.getUniformLocation(program, "u_sampler");

	$.getJSON('geometry.json', function(g_json) {
		// parse the json file
		g_data = parse_json(g_json);

		// load and setup the texture
		init_textures(gl);

		// initialize buffers using parsed data
		init_buffers(gl, g_data);

		// draw geometry
		var rate = 0.0;
		render();

		function render() {
			requestAnimationFrame(render);
			rate = animate(rate);
			drawScene(gl, g_data, rate);
		}
	});
}

/* ------------------------------------------
 * Parse JSON
 -------------------------------------------*/
function parse_json(json) {
	var v3 = vec3.create;

	// data to be passed to buffers for drawing
	var o = {};
	o.indices = [];
	o.vertices = [];
	o.vertex_normals = [];
	o.texture_coords = [];

	// vertex normal accumulator
	var acc = {};

	// parse indices
	for (var i = 0; i < json.faces.length; i++) {
		var face = json.faces[i];

		// face vertices
		var vrt1 = json.vertices[face[0]];
		var vrt2 = json.vertices[face[1]];
		var vrt3 = json.vertices[face[2]];

		// face vectors
		var v1 = vec3.sub(v3(), vrt2, vrt1);
		var v2 = vec3.sub(v3(), vrt3, vrt1);

		// face normal
		var n = vec3.normalize(v3(), vec3.cross(v3(), v1, v2));

		for (var ii = 0; ii < face.length; ii++) {
			var v_idx = face[ii];

			var acc_vrt_nrm = (acc[v_idx] || v3());
			acc[v_idx] = vec3.add(v3(), acc_vrt_nrm, n);
		}

		o.indices = o.indices.concat(face);
	}

	var y_max = 0;
	for (var k = 0; k < json.vertices.length; k++) {
		var candidate_y = json.vertices[k][1];
		if (candidate_y > y_max) {
			y_max = candidate_y;
		}
	}

	// parse vertices, normals and colors
	for (var j = 0; j < json.vertices.length; j++) {
		var vertex = json.vertices[j];

		// save vertex in format webgl can understand
		o.vertices = o.vertices.concat(vertex);

		// grab accumulated vertex normal, normalize, and store
		var vn = vec3.normalize(v3(), acc[j]);

		o.vertex_normals = o.vertex_normals.concat([vn[0], vn[1], vn[2]]);

		// compute texture coordinates
		var theta = Math.atan2(vertex[2], vertex[0]);
		var s = (theta + Math.PI) / (2 * Math.PI);
		var t = vertex[1] / y_max;

		o.texture_coords = o.texture_coords.concat([s, t]);
	}

	return o;
}

/* ------------------------------------------
 * Init textures
 -------------------------------------------*/
function init_textures(gl) {
	var program = gl.program;

	// geometry texture
	program.mozilla = gl.createTexture();
	var mozilla_image = new Image();
	mozilla_image.onload = function(texture, image) {
		return function() {
			gl.bindTexture(gl.TEXTURE_2D, texture);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,
					gl.UNSIGNED_BYTE, image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,
					gl.LINEAR_MIPMAP_NEAREST);
			gl.generateMipmap(gl.TEXTURE_2D);
			gl.bindTexture(gl.TEXTURE_2D, null);
		};
	} (program.mozilla, mozilla_image);
	mozilla_image.src = "brick.png";
}

/* ------------------------------------------
 * Init buffers
 -------------------------------------------*/
function init_buffers(gl, data) {
	var program = gl.program;

	// buffer for geometry vertices
	var vertex_buffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.vertices),
			gl.STATIC_DRAW);
	// sent as triple for (x, y, z)
	gl.vertexAttribPointer(program.a_Position, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(program.a_Position);

	// buffer for geometry texture coordinates
	var texture_coords_buffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, texture_coords_buffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.texture_coords),
			gl.STATIC_DRAW);
	// sent as double for (s, t)
	gl.vertexAttribPointer(program.a_textureCoord, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(program.a_textureCoord);

	// buffer for vertex normals
	var vertex_normal_buffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vertex_normal_buffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.vertex_normals),
			gl.STATIC_DRAW);
	// sent as triple for (x, y, z)
	gl.vertexAttribPointer(program.a_vertexNormal, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(program.a_vertexNormal);

	// buffer for geometry indices
	var index_buffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_buffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(data.indices),
			gl.STATIC_DRAW);
}

/* ------------------------------------------
 * Draw the scene
 -------------------------------------------*/
function drawScene(gl, g_data, rate) {
	var program = gl.program;

	gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	mat4.perspective(pMatrix, 45, gl.viewportWidth / gl.viewportHeight,
			0.1, 100.0);

	mat4.identity(mvMatrix);

	var t = Math.sin(degToRad(rate));

	var points = [];
	points[0] = [0, 1, 2];
	points[1] = [-3, 0, 0];
	points[2] = [-3, 1, -3];
	points[3] = [0, 2, -3];

	var point = bspline(t, points);

	var eye = [ point[0], point[1], point[2] ];
	var center = [0.0, 0.1, 0.0];
	var up = [0.0, 1.0, 0.0];

	mat4.lookAt(mvMatrix, eye, center, up);

	// load textures
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, program.mozilla);
	gl.uniform1i(program.u_sampler, 0);

	// generate normal matrix
	nMatrix = mat4.invert(mat4.create(), mvMatrix);
	mat4.transpose(nMatrix, nMatrix);

	// pass uniform matrices to fragment shader and draw
	setMatrixUniforms(gl);

	gl.drawElements(gl.TRIANGLES, g_data.indices.length,
			gl.UNSIGNED_SHORT, 0);
}

/* ------------------------------------------
 * Helper Functions
 -------------------------------------------*/

/* ------------------------------------------
 * Set Uniform Matrix
 -------------------------------------------*/
function setMatrixUniforms(gl) {
	var program = gl.program;

	gl.uniformMatrix4fv(program.u_mvMatrix, false, mvMatrix);
	gl.uniformMatrix4fv(program.u_pMatrix, false, pMatrix);
	gl.uniformMatrix4fv(program.u_nMatrix, false, nMatrix);
}

/* ------------------------------------------
 * Animation Logic
 -------------------------------------------*/
function animate(current_angle) {
	var now = Date.now();
	var elapsed = now - then;
	then = now;

	return (current_angle + (elapsed * ANGLE_STEP) / 1000.0) % 360;
}

/* ------------------------------------------
 * Retrieve shader source logic
 -------------------------------------------*/
function getShaderSource(id) {
	var shaderScript = $('#' + id);
	if (!shaderScript.get(0)) {
		return null;
	}

	return shaderScript.html();
}

/* ------------------------------------------
 * Simple degree to radians function
 -------------------------------------------*/
function degToRad(degree) {
	return degree * (Math.PI / 180.0);
}

/* ------------------------------------------
 * B-Spline
 -------------------------------------------*/
function bspline(t, p) {
	var a = ((-1/6)*Math.pow(t, 3) + (1/2)*Math.pow(t, 2) - (1/2)*t + (1/6));
	var b = ((1/2)*Math.pow(t, 3) - Math.pow(t, 2) + (2/3));
	var c = ((-1/2)*Math.pow(t, 3) + (1/2)*Math.pow(t, 2) + (1/2)*t + (1/6));
	var d = ((1/6)*Math.pow(t, 3));

	var p0 = p[0];
	p0[0] *= a;
	p0[1] *= a;
	p0[2] *= a;

	var p1 = p[1];
	p1[0] *= b;
	p1[1] *= b;
	p1[2] *= b;

	var p2 = p[2];
	p2[0] *= c;
	p2[1] *= c;
	p2[2] *= c;

	var p3 = p[3];
	p3[0] *= d;
	p3[1] *= d;
	p3[2] *= d;

	var retval = [0, 0, 0];
	retval[0] = p0[0] + p1[0] + p2[0] + p3[0];
	retval[1] = p0[1] + p1[1] + p2[1] + p3[1];
	retval[2] = p0[2] + p1[2] + p2[2] + p3[2];

	return retval;
}
