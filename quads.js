function Quad(tl, bl, br, tr) {
	/* tl  tr
	 *
	 * bl  br
	 *
	 * forward face = counter clockwise rotation, e.g. [tl, bl, br, tr]
	 */
	this.tl = tl;
	this.bl = bl;
	this.br = br;
	this.tr = tr;
}

Quad.prototype.toTriSing = function() {
	return [
		this.bl, this.br, this.tr,
		this.tr, this.tl, this.bl
	];
};

Quad.toTriMult = function(a) {
	val = [];

	for (var i = 0; i < a.length; i++) {
		var ct = a[i].toTriSing();

		val.push(ct[0]); val.push(ct[1]); val.push(ct[2]);
		val.push(ct[3]); val.push(ct[4]); val.push(ct[5]);
	}

	return val;
};

Quad.fromArrSing = function(a) {
	return new Quad(a[0], a[1], a[2], a[3]);
};

Quad.fromArrMult = function(a) {
	if (a.length % 4 !== 0) {
		throw "Array must be a multiple of 4.";
	}

	var val = [];
	var num_quads = a.length / 4;

	for (var i = 0; i < num_quads; i++) {
		var cq = i * 4;
		var ca = [a[cq], a[cq+1], a[cq+2], a[cq+3]];
		val.push(Quad.fromArrSing(ca));
	}

	return val;
};

/*
 * Tests
 */

console.log("create single quad from values");
var qv = new Quad(1, 2, 3, 4);
var ts_v = qv.toTriSing();
console.log(ts_v);

console.log("create single quad from array");
var qa = Quad.fromArrSing([1,2,3,4]);
var ts_a = qa.toTriSing();
console.log(ts_a);

console.log("create multiple quads from 'large' array");
var qm = Quad.fromArrMult([
	1, 2, 3, 4,
	5, 6, 7, 8,
	9, 10, 11, 12
]);
var ts_m = Quad.toTriMult(qm);
console.log(ts_m);
