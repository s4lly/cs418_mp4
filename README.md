# CS418: MP4 #

### Set up: ###
Loading of WebGL textures is subject to cross-domain access controls.
Therefore, I've provided two options for viewing the end result.

> One, Install [nodejs](http://www.nodejs.org) then run the following command in your terminal before opening your browser to localhost:8080:
```
npm install
node server.js
```

> Two, Visit the provided link [here](web.engr.illinois.edu/~jmercad2/cs418/mp4/)

It is important to use a WebGL supported browser. See the [link](http://www.khronos.org/webgl/wiki/Getting_a_WebGL_Implementation)
for information on currently supported browsers.

### Video ###
[video link](http://youtu.be/zSEeSN3YmQM)
