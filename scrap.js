
/*var vert1 = {x: 1, y: 2, z: 3};
var vert2 = {x: 4, y: 5, z: 6};

var edge1 = {end: vert1};
var edge2 = {end: vert2};

console.log(edge1);
console.log(edge2);

console.log("changed edge1.end.x into 9");
edge1.end.x = 9;

console.log(edge1);
console.log(vert1);

console.log("changed vert 1 y into 9999999");
vert1.y = 9999999;

console.log(vert1);
console.log(edge1);*/

// Start of mp4 code and stuff

// Data (from worksheet 5)
// basically vertices
var d1 = [1, 2, 2];
var d2 = [2, 2, -2];
var d3 = [2, 0, -2];
var d4 = [1, 0, 2];
var d5 = [0, 0, -2];
var d6 = [0, 2, -2];
var d7 = [0, 4, -2];
var d8 = [1, 4, 2];
var d9 = [2, 4, -2];

// The var g specifies the faces for a particular shape,
// each inner array basically a face
var g = [
	[d5, d6, d1, d4],
	[d4, d1, d2, d3],
	[d6, d7, d8, d1],
	[d1, d8, d9, d2]
];

// Classes
function HalfEdge(opp, next, end, left) {
	this.opp  = null || opp;
	this.next = null || next;
	this.end  = null || end;
	this.left = null || left;

	/*opp: null,  // HE
	next: null, // HE
	end: null,  // V
	left: null  // F*/
}

function Vertex(x, y, z, edge) {
	this.x = null || x;
	this.y = null || y;
	this.z = null || z;

	this.edge = null || edge;

	/*x: undefined,
	y: undefineda);
	z: undefined,
	edge: null*/
}

Vertex.sum = function(o, a, b) {
	o.x = a.x + b.x;
	o.y = a.y + b.y;
	o.z = a.z + b.z;

	return o;
};

Vertex.mulByScalar = function(o, a, s) {
	o.x = a.x * s;
	o.y = a.y * s;
	o.z = a.z * s;

	return o;
};

Vertex.divByScalar = function(o, a, s) {
	o.x = a.x / s;
	o.y = a.y / s;
	o.z = a.z / s;

	return o;
};

function Face(edge) {
	this.edge = edge;

	/*edge: null*/
}

// Helper functions
function genFaceEdges(f) {
	// assuming f = array of length four
	// with components [v1, v2, v3, v4]
	// and order given specifies forward
	// facing orientation of the face (cc)
	var val = [];

	for (var i = 0; i < 4; i++) {
		var ci = i;
		var ni = (i+1) % 4;

		var ce = [f[ci], f[ni]];

		val.push(ce);
	}

	return val;
}

// Exposing helpers for testing
module.exports.HalfEdge = HalfEdge;
module.exports.Vertex = Vertex;
module.exports.Face = Face;
module.exports.genFaceEdges = genFaceEdges;

// Constructing Half-Edge structures from indexed face set
/*var num_faces = g.length;
var Edges = {};

for (var fi = 0; fi < num_faces; fi++) {
	var f = g[fi]; // TODO: going to need to use Face object and not array
	var es = genFaceEdges(f);
	var num_edges = es.length;

	for (var ei = 0; ei < num_edges; ei++) {
		var ek = es[ei].toString();

		Edges[ek] = new HalfEdge();
		Edges[ek].face = f;
	}

	for (ei = 0; ei < num_edges; ei++) {
	}
}*/










// dirty, check if all edges have their .opp assigned,
// provide pair for edges incident to only one face
for (var ek in eos) {
	if (!eos.hasOwnProperty(ek)) {
		// the current property is not a direct property of eos
		continue;
	}

	// the current edge object does not contain a reference to opp
	if (!eos[ek].opp) {
		// reference to edge missing an opposite
		var emo = eos[ek];

		var nhe = new HalfEdge();
		emo.opp = nhe;
		nhe.opp = emo;

		var it = emo.next;
		while (it.next !== emo) {
			it = it.next;
		}

		nhe.next = it.opp;
		nhe.end = it.end;

		// nhe.left will remain undefined
	}
}



// wksht 5
var vertices = [
	1, 2, 2,
	2, 2, -2,
	2, 0, -2,
	1, 0, 2,
	0, 0, -2,
	0, 2, -2,
	0, 4, -2,
	1, 4, 2,
	2, 4, -2
];



// wksht 5
var indices = [
	4, 5, 0, 3,
	3, 0, 1, 2,
	5, 6, 7, 0,
	0, 7, 8, 1
];




/*function constructVertArr(nvs, vos, fos, eos) {
	// new vertices' index
	var nvsi = 0;

	processVertexObjects();

	function processVertexObjects() {
		for (var vk in vos) {
			if (!vos.hasOwnProperty(vk)) { continue; }
			var v = vos[vk];

			// new vertex data
			var nvd = [v.x, v.y, v.z]
			// new vertex key
			var nvk = nvd.toString();

			nvs[nvk] = nvsi
		}
	}
}*/




// face objects
var fos = {};
// number of faces
var nfs = indices.length / 4;

for (var fi = 0; fi < nfs; fi++) {
	// start of fi'th vertex
	var sif = 4*fi;
	fos[fi] = {
		obj: new Face(),
		fvs: []
	};
}


/*var vs = [];
var num_vertices = vertices.length;
for (var vi = 0; vi < num_vertices; vi++) {
	var cv = 4*vi;

	var v = new Vertex(
		vertices[cv], vertices[cv+1],
		vertices[cv+2], vertices[cv+3],
		vi
	);

	vs.push(v);
}*/

/*var fs = [];
var num_faces = indices.length / 4;
for (var fi = 0; fi < num_faces; fi++) {
	var fvs = [];

	for (var fvsi = 0; fvsi < 4; fvsi++) {
		var cfvs = 4*fi;

		fvs.push(4*fi
	}
}*/

