function HalfEdge(opp, next, end, left) {
	this.opp  = null || opp;
	this.next = null || next;
	this.end  = null || end;
	this.left = null || left;

	/*opp: null,  // HE
	next: null, // HE
	end: null,  // V
	left: null  // F*/
}

function Vertex(x, y, z, edge) {
	this.x = null || x;
	this.y = null || y;
	this.z = null || z;

	this.edge = null || edge;

	/*x: undefined,
	y: undefineda);
	z: undefined,
	edge: null*/
}

Vertex.sum = function(o, a, b) {
	o.x = a.x + b.x;
	o.y = a.y + b.y;
	o.z = a.z + b.z;

	return o;
};

Vertex.mulByScalar = function(o, a, s) {
	o.x = a.x * s;
	o.y = a.y * s;
	o.z = a.z * s;

	return o;
};

Vertex.divByScalar = function(o, a, s) {
	o.x = a.x / s;
	o.y = a.y / s;
	o.z = a.z / s;

	return o;
};

function Face(edge) {
	this.edge = edge;

	/*edge: null*/
}

function subdivide(vertices, indices) {
	// #### Main

	// #Globals
	var vos = {}; // vertex objects
	var fos = {}; // face objects
	var eos = {}; // edge objects

	var svs =[];   // subdivided vertices
	var sqis = []; // subdivided quad indices
	var stis = []; // subdivided triangle indices

	constructHalfEdgeStruct();
	genSubdivisionPoints();
	genQuadIndices();
	genTriangleIndices();

	//console.log(sqis);

	return {'vertices': svs, 'quadIndices': sqis, 'triangleIndices': stis};

	// #### Helper Function definitions
	function constructHalfEdgeStruct() {
		processVertices();
		processIndices();

		function processVertices() {
			// #Create vertices
			// number of vertices
			var svs = vertices.length / 3;

			for (var vi = 0; vi < svs; vi++) {
				// start of i'th vertex's components (in this case i = vi for 'vertex index')
				var sv = 3*vi;
				vos[vi] = new Vertex(vertices[sv], vertices[sv+1], vertices[sv+2]);
			}
		}

		function processIndices() {
			// #Create half-edges, and in turn faces
			// number of faces
			var nfs = indices.length / 4;

			for (var fi = 0; fi < nfs; fi++) {
				// start of i'th face's vertices (in this case i = fi for 'face index')
				var sfv = 4*fi;
				// array of i'th face's edges, four total each an array of vertex pairs
				var fes = [];

				for (var fvi = 0; fvi < 4; fvi++) {
					// first and second vertex pair
					var fvp = fvi;
					var svp = (fvi+1) % 4;

					fes.push([indices[sfv+fvp], indices[sfv+svp]]);
				}

				// create face
				var f = new Face();
				fos[fi] = f;
				// number of edges for current face (should always be 4)
				var nfes = fes.length;

				for (var ei = 0; ei < nfes; ei++) {
					var eki = fes[ei].toString();

					eos[eki] = new HalfEdge();
					eos[eki].left = f;
					eos[eki].end = vos[fes[ei][1]];
				}

				// assign .edge of face to arbitrary bordering edge (here, first created)
				f.edge = eos[fes[0].toString()];

				for (var ej = 0; ej < nfes; ej++) {
					var ekj = fes[ej].toString();

					// ##assign .edge to bordering face vertices if not done so yet

					// select vertex used as edge's starting point
					var vo = vos[fes[ej][0]];
					if (!vo.edge) {
						vo.edge = eos[ekj];
					}

					// ##assign .next and .opp to edge

					// next edge index
					var nei = (ej+1) % 4;
					// key for next edge in eos
					var nek = fes[nei].toString();

					eos[ekj].next = eos[nek];

					// opposite edge key
					var oek = [fes[ej][1], fes[ej][0]].toString();

					// if opposite edge exists, assign .opp of current edge to it and vice versa
					if (eos[oek]) {
						eos[ekj].opp = eos[oek];
						eos[oek].opp = eos[ekj];
					}
				}
			}
		}
	}

	function genSubdivisionPoints() {
		// global new vertex index
		var gnvi = 0;

		genFacePoints();
		genEdgePoints();
		genNewPoints();

		function addPoint(point) {
			svs.push(point.x, point.y, point.z);
			return {'v': point, 'i': gnvi++};
		}

		function genFacePoints() {
			// #for each face, add a face-point

			for (var fk in fos) {
				if (!fos.hasOwnProperty(fk)) { continue; }

				var vs = [];

				var f = fos[fk];
				var e = f.edge;
				vs.push(e.end);

				var it = e.next;
				while (it !== e) {
					vs.push(it.end);
					it = it.next;
				}

				var nfvs = vs.length;
				var fp = new Vertex(0, 0, 0);

				for (var i = 0; i < nfvs; i++) {
					Vertex.sum(fp, fp, vs[i]);
				}

				Vertex.divByScalar(fp, fp, nfvs);

				f.facePoint = addPoint(fp);
			}
		}

		console.log(fos[0].facePoint.v);

		function genEdgePoints() {
			// #for each edge, add an edge-point

			for (var hek in eos) {
				if (!eos.hasOwnProperty(hek)) { continue; }
				var he = eos[hek];

				if (he.opp.edgePoint) {
					he.edgePoint = he.opp.edgePoint;
					continue;
				}

				// two incident vertices
				var tp = he.end;
				var ip = he.opp.end;

				// two face points from incident faces
				var fp1 = he.left.facePoint;
				var fp2 = he.opp.left.facePoint;

				// calculate edge point
				var ep = new Vertex(0, 0, 0);
				Vertex.sum(ep, ep, tp);
				Vertex.sum(ep, ep, ip);
				Vertex.sum(ep, ep, fp1);
				Vertex.sum(ep, ep, fp2);
				Vertex.divByScalar(ep, ep, 4);

				he.edgePoint = addPoint(ep);
			}
		}

		function genNewPoints() {
			// #for each original point:

			for (var vk in vos) {
				if (!vos.hasOwnProperty(vk)) { continue; }
				//console.log("for vertex: " + vk);
				var v = vos[vk];

				// collect current point's adjacent faces (adjfs) and edges (adjes)
				var adjfs = [];
				var adjes = [];

				var he = v.edge;
				adjfs.push(he.left);
				adjes.push(he);

				var it = he.opp.next;
				while (it !== he) {
					adjfs.push(it.left);
					adjes.push(it);
					it = it.opp.next;
				}

				// #average F of all n face points for faces touching v
				var nadjfs = adjfs.length;
				//console.log('    num adj fs:' + nadjfs);
				var F = new Vertex(0, 0, 0);

				for (var i = 0; i < nadjfs; i++) {
					Vertex.sum(F, F, adjfs[i].facePoint);
				}

				Vertex.divByScalar(F, F, nadjfs);

				// average R of all n edge midpoints for edges touching v
				var nadjes = adjes.length; // valence
				//console.log('    num adj es:' + nadjes);
				var R = new Vertex(0, 0, 0);

				for (var j = 0; j < nadjes; j++) {
					var tp = adjes[j].end;
					var ip = adjes[j].opp.end;

					var mp = new Vertex(0, 0, 0);
					Vertex.sum(mp, tp, ip);
					Vertex.divByScalar(mp, mp, 2);

					Vertex.sum(R, R, mp);
				}

				Vertex.divByScalar(R, R, nadjes);

				// generate new point, via (F + 2*R + (n-3)*P) / n
				var np = new Vertex(0, 0, 0);

				Vertex.sum(np, np, F);

				Vertex.mulByScalar(R, R, 2);
				Vertex.sum(np, np, R);

				// TODO: Might have to change, n=valence
				// (n-3)*P, since n=4 skip this step
				Vertex.sum(np, np, v);

				Vertex.divByScalar(np, np, 4);

				v.newPos = addPoint(np);
			}
		}
	}

	function genQuadIndices() {
		for (var fk in fos) {
			if (!fos.hasOwnProperty(fk)) { continue; }
			// for every face
			var f = fos[fk];

			// iterate over its half-edges
			var he = f.edge;
			var it = he;

			// and connect appropriate points
			do {
				sqis.push(
						it.edgePoint.i,
						f.facePoint.i,
						it.opp.next.edgePoint.i,
						it.opp.end.newPos.i
				);

				it = it.next;
			} while (it !== he);
		}
	}

	function genTriangleIndices() {
		for (var i = 0; i < sqis.length/4; i++) {
			var start = 4*i;
			//var qis = sqis[i];

			// TODO: might be problomatic
			stis.push(sqis[start], sqis[start+1], sqis[start+3]);
			stis.push(sqis[start+1], sqis[start+2], sqis[start+3]);
		}
	}
}

// unit cube
var vertices = [
	-1,  1,  1,
	-1, -1,  1,
	 1, -1,  1,
	 1,  1,  1,
	-1,  1, -1,
	-1, -1, -1,
	 1, -1, -1,
	 1,  1, -1
];

// unit cube
var indices = [
	0, 1, 2, 3,
	7, 6, 5, 4,
	4, 5, 1, 0,
	6, 7, 3, 2,
	4, 0, 3, 7,
	1, 5, 6, 2
];

var data1 = subdivide(vertices, indices);
console.log(data1.vertices.length / 3);
console.log(data1.triangleIndices.length/3);

var data2 = subdivide(data1.vertices, data1.quadIndices);
console.log(data2.vertices.length / 3);
console.log(data2.triangleIndices.length / 3);
